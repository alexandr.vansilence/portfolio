
Shader "HPEW/TwistPerson"
{
	Properties
	{
		_MainTex("Sprite Texture", 2D) = "white" {}
		_TwistBend("_TwistBend", Range(-1, 1)) = 0.296
		_TwistPosX("_TwistPosX", Range(-1, 2)) = 0.5
		_TwistPosY("_TwistPosY", Range(-1, 2)) = 0.5
		_TwistRadius("_TwistRadius", Range(0, 1)) = 0.5
		//_LerpUV_Fade_1("_LerpUV_Fade_1", Range(0, 1)) = 1
		//_SpriteFade("SpriteFade", Range(0, 1)) = 1.0
	}

	SubShader
	{
		Tags {"Queue" = "Transparent" "IgnoreProjector" = "true" "RenderType" = "Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				fixed4 vertex   : POSITION;
				fixed4 color    : COLOR;
				fixed2 texcoord : TEXCOORD0;
			};
			
			struct v2f
			{
				fixed4 texcoord : TEXCOORD0;
				fixed4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
			};
			
			sampler2D _MainTex;
			fixed _TwistBend;
			fixed _TwistPosX;
			fixed _TwistPosY;
			fixed _TwistRadius;
			
			v2f vert(appdata_t i)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(i.vertex);
				o.texcoord.zw = fixed2(_TwistPosX, _TwistPosY);
				o.texcoord.xy = i.texcoord - o.texcoord.zw;
				o.color = i.color;
				o.color.a *= 1 - abs(_TwistBend);
				return o;
			}

			fixed4 frag (v2f i) : COLOR
			{
				fixed2 TwistUV = i.texcoord.xy;
				fixed dist = length(TwistUV);

				if (dist < _TwistRadius)
				{
					fixed percent = 1 - dist / _TwistRadius;
					fixed theta = percent * percent * 16.0 * _TwistBend;
					fixed2 mat = fixed2(sin(theta), cos(theta));
					TwistUV = fixed2(TwistUV.x * mat.y + TwistUV.y * (-mat.x), TwistUV.x * mat.x + TwistUV.y * mat.y);
				}

				TwistUV += i.texcoord.zw;

				fixed4 FinalResult = tex2D(_MainTex, TwistUV) * i.color;
				return FinalResult;
			}
		
			ENDCG
		}
	}

	Fallback "Sprites/Default"
}
