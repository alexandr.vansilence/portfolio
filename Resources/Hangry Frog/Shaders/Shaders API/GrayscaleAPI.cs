using UnityEngine;
using UnityEngine.UI;

public class GrayscaleAPI : MonoBehaviour
{
    [SerializeField] private Material material;

    private enum ShaderType { Colored = 0, Grayscale = 1 }

    public void Gray()
    {
        ChangeColorize(material, ShaderType.Grayscale);
    }

    public void Colored()
    {
        ChangeColorize(material, ShaderType.Colored);
    }

    public void GrayImage(Image image)
    {
        ImageControl(image, ShaderType.Grayscale);
    }

    public void ColoredImage(Image image)
    {
        ImageControl(image,ShaderType.Colored);
    }

    private void ImageControl(Image image, ShaderType shaderType)
    {
        if (image != null && image.material != null)
        {
            Material material = Instantiate(image.material);
            ChangeColorize(material, shaderType);
            image.material = material;
        }
        else
        {
            Debug.LogError("Image or her material is null");
        }
    }

    private void ChangeColorize(Material material, ShaderType shaderType)
    {
        if (material != null)
        {
            material.SetInt("_UseUIGray", (int)shaderType);
        }
        else
        {
            Debug.LogError("Material is null");
        }
    }
}