Shader "Unlit/Sci-Fi Shield"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _Emission ("Emission", Float) = 1
        _Opacity ("Opacity", Float) = 1
        _FresnelPower("Frensel power", Float) = 1
        _FresnelColor("Frensel color", Color) = (1,1,1,1)
        _NoiseSpeed ("Noise speed",Float) = 0.3
        _VertexOffset ("Vertex offset", Float) = 0.15
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent" "IgnoreProjector" = "true" }

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma target 2.0

            #include "UnityCG.cginc"

            struct appdata
            {
                fixed4 vertex : POSITION;
                fixed3 normal : NORMAL;
                fixed2 uv : TEXCOORD0;
                fixed4 vertexColor : COLOR;
            };

            fixed _NoiseSpeed;
            fixed _VertexOffset;
            fixed _FresnelPower;
            fixed4 _Color;
            fixed4 _FresnelColor;
            fixed _Emission;
            fixed _Opacity;

            struct v2f
            {
                fixed4 vertex : SV_POSITION;
                fixed4 vertexColor : COLOR;
                fixed4 uv : TEXCOORD0;
                fixed3 frenselcol : TEXCOORD1;
            };

            void Unity_Rotate_Radians_float(fixed2 UV, fixed2 Center, fixed Rotation, out fixed2 Out)
            {
                UV -= Center;
                fixed s = sin(Rotation);
                fixed c = 1 - s * s;
                fixed2x2 rMatrix = fixed2x2(c, -s, s, c);
                UV.xy = mul(UV.xy, rMatrix);
                Out = UV + Center;
            }

            fixed2 unity_gradientNoise_dir(fixed2 p)
            {
                fixed x = 34 * p.x * p.x % 289 + p.y;
                x = 34 * x * x % 289;
                x = frac(x / 41);
                x = x + x;
                return normalize(fixed2(x - floor(x + 0.5h), x - 0.5h));
            }

            fixed unity_gradientNoise(fixed2 p)
            {
                fixed2 ip = floor(p);
                fixed2 fp = frac(p);
                fixed2 mat = fixed2(0, 1);
                fixed d00 = dot(unity_gradientNoise_dir(ip), fp);
                fixed d01 = dot(unity_gradientNoise_dir(ip + mat), fp - mat);
                fixed d10 = dot(unity_gradientNoise_dir(ip + mat.yx), fp - mat.yx);
                fixed d11 = dot(unity_gradientNoise_dir(ip + (fixed2)1), fp - (fixed2)1);
                fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
                return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
            }

            sampler2D _MainTex;
            fixed4 _MainTex_ST;

            // uv.z - noise
            // uv.w - fresnel
            v2f vert (appdata v)
            {
                v2f o;

                fixed timeScale = _NoiseSpeed * _Time.x;
                fixed2 rotate;

                Unity_Rotate_Radians_float(v.normal.xy, (fixed2)0.5h, timeScale, rotate);
                rotate += (fixed2)timeScale;
                o.uv.z = unity_gradientNoise(rotate + rotate) + 0.5h;

                fixed3 vertexOffset = v.normal.xyz * (fixed3)o.uv.z *  (fixed3)_VertexOffset;
                o.uv.z = o.uv.z * 1.5h;

                fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
                fixed3 viewDir = ObjSpaceViewDir(v.vertex);

                o.uv.w = pow(1.0h - saturate(dot(worldNormal, normalize(viewDir))), _FresnelPower);
                o.vertexColor = v.vertexColor;
                o.vertexColor.rgb *= _Emission;
                o.vertexColor.a *= _Opacity;
               
                o.vertex = UnityObjectToClipPos(v.vertex + vertexOffset);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
                o.frenselcol = o.uv.w * _FresnelColor;
                return o;
            }

            // uv.z - noise
            // uv.w - fresnel
            fixed4 frag(v2f i) : SV_Target
            {
                fixed  tex = tex2D(_MainTex, i.uv.xy).r * i.uv.z - 0.5h;
                fixed3 col = clamp(tex , 0.0h, 0.5h) * _Color + i.frenselcol;
                return fixed4(col, clamp(i.uv.w + tex, 0.0h, 1.0h)) * i.vertexColor;
            }

            ENDCG
        }
    }
}
