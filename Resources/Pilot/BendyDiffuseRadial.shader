// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/Bendy diffuse - Radial" 
{
	Properties 
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert addshadow
		#pragma multi_compile __ HORIZON_WAVES 
		#pragma multi_compile __ BEND_ON

		// Global properties to be set by BendControllerRadial script
		uniform half3 _CurveOrigin;
		uniform fixed3 _ReferenceDirection;
		uniform half _Curvature;
		uniform fixed3 _Scale;
		uniform half _FlatMargin;
		uniform half _HorizonWaveFrequency;

		// Per material properties
		sampler2D _MainTex;
		fixed4 _Color;

		struct Input 
		{
			//float2 uv_MainTex;
			float3 worldPos;
			float3 normal;
			float offset : TEXCOORD0;
		};

		void vert(inout appdata_full v, out Input o)
		{
			//calculate world position of vertex
			float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
			
			//calculate world normal
			float3 worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
			o.normal = normalize(worldNormal);
			float offsetY = 0;
		
			#if defined(BEND_ON)
			half4 wpos = mul(unity_ObjectToWorld, v.vertex);

			half2 xzDist = (wpos.xz - _CurveOrigin.xz) / _Scale.xz;
			half dist = length(xzDist);
			fixed waveMultiplier = 1;

			#if defined(HORIZON_WAVES)
			half2 direction = lerp(_ReferenceDirection.xz, xzDist, min(dist, 1));

			half theta = acos(clamp(dot(normalize(direction), _ReferenceDirection.xz), -1, 1));

			waveMultiplier = cos(theta * _HorizonWaveFrequency);
			#endif

			dist = max(0, dist - _FlatMargin);

			offsetY = dist * dist * _Curvature * waveMultiplier;

			wpos.y -= offsetY;
			
			wpos = mul(unity_WorldToObject, wpos);

			v.vertex = wpos;
			#endif

			o.offset = offsetY;

			o.worldPos = worldPos.xyz;
		}

		float4 _MainTex_ST;

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 uv_front = TRANSFORM_TEX(float2(IN.worldPos.x, IN.worldPos.y + IN.offset), _MainTex);
			float2 uv_side = TRANSFORM_TEX(float2(IN.worldPos.z, IN.worldPos.y + IN.offset), _MainTex);
			float2 uv_top = TRANSFORM_TEX(IN.worldPos.xz, _MainTex);

			//read texture at uv position of the three projections
			fixed4 col_front = tex2D(_MainTex, uv_front);
			fixed4 col_side = tex2D(_MainTex, uv_side);
			fixed4 col_top = tex2D(_MainTex, uv_top);

			//generate weights from world normals
			float3 weights = IN.normal;
			//show texture on both sides of the object (positive and negative)
			weights = abs(weights);
			//make the transition sharper
			weights = pow(weights, 1);
			//make it so the sum of all components is 1
			weights = weights / (weights.x + weights.y + weights.z);

			//combine weights with projected colors
			col_front *= weights.z;
			col_side *= weights.x;
			col_top *= weights.y;

			//combine the projected colors
			fixed4 c = col_front + col_side + col_top;

			c *= _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}

	Fallback "Legacy Shaders/Diffuse"
}
