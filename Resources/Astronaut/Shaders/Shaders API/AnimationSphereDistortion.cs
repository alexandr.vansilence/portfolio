using System;
using System.Collections;
using UnityEngine;

namespace Kudos.ShadersAPI
{
    public class AnimationSphereDistortion : MonoBehaviour
    {

        [SerializeField]
        float speedDisappear = 1f; // �������� ������������ �����

        [SerializeField]
        float sizeDistortion = 2f; // ������ ��������� ������������ ���������� �������

        Material material;
        void Awake()
        {
            material = GetComponent<Renderer>().material;
        }

        /// <summary>
        /// �������, ����������� �������� "������" �����
        /// </summary>
        public void DistortionSphere(Action callback = null)
        {
            StartCoroutine(AnimationDisappear(callback));
        }

        /// <summary>
        /// �������, �������� ������� ������� �����
        /// </summary>
        /// <param name="rim"></param>
        public void RimChange(float rim)
        {
            material.SetFloat("_FresnelWidth", rim);
        }

        IEnumerator AnimationDisappear(Action callback = null)
        {
            Color color = material.GetColor("_Color");
            color.a = 0;
            material.SetColor("_Color",color);
            float startSize = transform.localScale.x;

            while (material.GetFloat("_FresnelWidth") > 0)
            {
                RimChange(material.GetFloat("_FresnelWidth") - speedDisappear / 20);
                if (material.GetFloat("_FresnelWidth") < 0) RimChange(0);
                yield return null;
            }

            while (transform.localScale.x < startSize * sizeDistortion)
            {
                transform.localScale *= 1 + (speedDisappear / 20);
                yield return null;
            }

            while (transform.localScale.x > 0.1)
            {
                transform.localScale *= 1 - (speedDisappear / 5);
                yield return null;
            }

            callback?.Invoke();
        }
    }
}
