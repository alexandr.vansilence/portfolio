using UnityEngine;

namespace Kudos.ShadersAPI
{
    /// <summary>
    /// ����� ��� ������ �������� ���� �������� ��������� �� ������
    /// </summary>
    public class PostProcessingCamera : MonoBehaviour
    {
        [SerializeField] private Material postProcessingMaterial;

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Graphics.Blit(source, destination, postProcessingMaterial);
        }

        /// <summary>
        /// ����� ��� ���������� ������
        /// </summary>
        /// <param name="dark">������� ���������� ������ � �������� �� 0 �� 1</param>
        public void ChangeDarknessCamera(float dark = 0)
        {
            postProcessingMaterial.SetFloat("_StrengthDark", dark);
        }
    }
}
