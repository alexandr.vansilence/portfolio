﻿Shader "Hidden/Kudos/FastMobileBloom"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_BloomTex("Bloom (RGB)", 2D) = "black" {}
	}

	CGINCLUDE

		#include "UnityCG.cginc"	
		#pragma target 2.0

		uniform sampler2D _MainTex;
		uniform fixed4	  _MainTex_TexelSize;
		uniform	fixed4	  _MainTex_ST;
		uniform fixed	  _ThresholdParams;
		uniform fixed	  _Spread;
		uniform sampler2D _BloomTex;
		uniform fixed	  _BloomIntensity;

		struct v2fCombineBloom
		{
			fixed4 pos : SV_POSITION;
			fixed2  uv  : TEXCOORD0;
			#if UNITY_UV_STARTS_AT_TOP
			fixed2  uv2 : TEXCOORD1;
			#endif
		};

		struct v2fBlurDown
		{
			fixed4 pos  : SV_POSITION;
			fixed2  uv0  : TEXCOORD0;
			fixed4  uv12 : TEXCOORD1;
			fixed4  uv34 : TEXCOORD2;
		};

		struct v2fBlurUp
		{
			fixed4 pos  : SV_POSITION;
			fixed4  uv12 : TEXCOORD0;
			fixed4  uv34 : TEXCOORD1;
			fixed4  uv56 : TEXCOORD2;
			fixed4  uv78 : TEXCOORD3;
		};

		v2fBlurDown vertBlurDown(appdata_img v)
		{
			v2fBlurDown o;
			fixed2 texSize = _MainTex_TexelSize.xy * _Spread;
			fixed2 minusOneOne = fixed2(-texSize.x, texSize.y);
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv0 = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy, _MainTex_ST);
			o.uv12.xy = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy + texSize, _MainTex_ST);
			o.uv12.zw = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy + minusOneOne, _MainTex_ST);
			o.uv34.xy = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy - texSize, _MainTex_ST);
			o.uv34.zw = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy - minusOneOne, _MainTex_ST);
			return o;
		}

		v2fBlurUp vertBlurUp(appdata_img v)
		{
			v2fBlurUp o;
			fixed2 texSize = _MainTex_TexelSize.xy * _Spread;
			fixed2 minusOneOne = fixed2(-texSize.x, texSize.y);
			fixed2 zeroTwo = fixed2(0.0h, texSize.y + texSize.y);
			fixed2 twoZero = fixed2(texSize.x + texSize.x, 0.0h);
			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv12.xy = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy + texSize, _MainTex_ST);
			o.uv12.zw = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy + minusOneOne, _MainTex_ST);
			o.uv34.xy = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy - texSize, _MainTex_ST);
			o.uv34.zw = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy - minusOneOne, _MainTex_ST);
			o.uv56.xy = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy + zeroTwo, _MainTex_ST);
			o.uv56.zw = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy - zeroTwo, _MainTex_ST);
			o.uv78.xy = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy + twoZero, _MainTex_ST);
			o.uv78.zw = UnityStereoScreenSpaceUVAdjust(v.texcoord.xy - twoZero, _MainTex_ST);
			return o;
		}

		v2fCombineBloom vertCombineBloom(appdata_img v)
		{
			v2fCombineBloom o;

			o.pos = UnityObjectToClipPos(v.vertex);
			o.uv = UnityStereoScreenSpaceUVAdjust(v.texcoord, _MainTex_ST);
			#if UNITY_UV_STARTS_AT_TOP
				o.uv2 = o.uv;
				if (_MainTex_TexelSize.y < 0.0h)
					o.uv.y = 1.0h - o.uv.y;
			#endif

			return o;
		}

		fixed4 fragBlurDownFirstPass(v2fBlurDown i) : SV_Target
		{
			fixed4 col0 = tex2D(_MainTex, i.uv0);
			fixed4 col1 = tex2D(_MainTex, i.uv12.xy);
			fixed4 col2 = tex2D(_MainTex, i.uv12.zw);
			fixed4 col3 = tex2D(_MainTex, i.uv34.xy);
			fixed4 col4 = tex2D(_MainTex, i.uv34.zw);

			fixed4 col = (col0 * 4.0h + col1 + col2 + col3 + col4) * 0.125h + _ThresholdParams.x;

			//col = max(col, 0.0);
			return col;
		}

		fixed4 fragBlurDown(v2fBlurDown i) : SV_Target
		{
			fixed4 col0 = tex2D(_MainTex, i.uv0);
			fixed4 col1 = tex2D(_MainTex, i.uv12.xy);
			fixed4 col2 = tex2D(_MainTex, i.uv12.zw);
			fixed4 col3 = tex2D(_MainTex, i.uv34.xy);
			fixed4 col4 = tex2D(_MainTex, i.uv34.zw);

			fixed4 col = (col0 * 4.0h + col1 + col2 + col3 + col4) * 0.125h;
			return col;
		}

		fixed4 fragBlurUp(v2fBlurUp i) : SV_Target
		{
			fixed4 col1 = tex2D(_MainTex, i.uv12.xy);
			fixed4 col2 = tex2D(_MainTex, i.uv12.zw);
			fixed4 col3 = tex2D(_MainTex, i.uv34.xy);
			fixed4 col4 = tex2D(_MainTex, i.uv34.zw);
			fixed4 col5 = tex2D(_MainTex, i.uv56.xy);
			fixed4 col6 = tex2D(_MainTex, i.uv56.zw);
			fixed4 col7 = tex2D(_MainTex, i.uv78.xy);
			fixed4 col8 = tex2D(_MainTex, i.uv78.zw);

			fixed4 col = (col1 + col2 + col3 + col4) * 0.3333333h + (col5 + col6 + col7 + col8) * 0.16666666h;

			return col;
		}

		fixed4 fragCombineBloom(v2fCombineBloom i) : SV_Target
		{
			#if UNITY_UV_STARTS_AT_TOP
				fixed4 col = tex2D(_MainTex,  i.uv2);
			#else
				fixed4 col = tex2D(_MainTex,  i.uv);
			#endif
			return col + tex2D(_BloomTex, i.uv) * _BloomIntensity;
		}

	ENDCG

	SubShader
	{
		Cull Off ZWrite Off ZTest Always

			//initial downscale and threshold
		Pass
		{
			CGPROGRAM
			#pragma vertex vertBlurDown
			#pragma fragment fragBlurDownFirstPass
			ENDCG
		}

			//down pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vertBlurDown
			#pragma fragment fragBlurDown
			ENDCG
		}

			//up pass
		Pass
		{
			CGPROGRAM
			#pragma vertex vertBlurUp
			#pragma fragment fragBlurUp
			ENDCG
		}

			//final bloom
		Pass
		{
			CGPROGRAM
			#pragma vertex vertCombineBloom
			#pragma fragment fragCombineBloom
			ENDCG
		}
	}
}