Shader "HPEW/Line render effect"
{
    Properties
    {
        [HDR]_Color("Color", Color) = (1,1,1,1)
        _MainTex ("Particles texture" , 2D) = "white" {}
        _Gradient("Gradient", 2D) = "black" {}
        _TransparentAmount("Transparent amount", Float) = 0
        _Speed("Speed",Float) = 0

        _Distantion("Clip value", Float) = 1

    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        LOD 100

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite ON

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #pragma target 2.0

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 worldPos : TEXCOORD1;
                float transpatentAmount : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _Gradient;
            float4 _Gradient_ST;
            float _TransparentAmount;
            fixed4 _Color;
            float _Distantion;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv.zw = TRANSFORM_TEX(v.uv, _Gradient);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.uv.x += _Time.y * _Speed;
                o.transpatentAmount = 1 + _TransparentAmount;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv.xy) * _Color;
                
                fixed grad = i.transpatentAmount - tex2D(_Gradient, i.uv.zw).r;
                
                col.a = max(0.15f, grad);
             
                if (i.uv.w <= 0.1f)
                {
                    col.a += 1;
                    col.rgb = lerp( col.rgb + 0.9f , col.rgb, i.uv.w * 10);
                }
                else if (i.uv.w >= 0.9f)
                {
                    col.a += 1;
                    col.rgb = lerp(col.rgb + 0.5f , col.rgb, (1 - i.uv.w) * 10);
                }

                float dist = distance(i.worldPos, _WorldSpaceCameraPos) - _Distantion;
                col.a *= saturate(dist);
                
                return col;
            }
            ENDCG
        }
    }
}
