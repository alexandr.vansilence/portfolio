Shader "Kudos/Distortion NC"
{
	Properties
	{
		[HDR] _Color("MainColor", Color) = (1,1,1,1)
		_Fresnel("Fresnel Intensity", Range(0,200)) = 3.0
		_FresnelWidth("Fresnel Width", Range(0,2)) = 3.0
		_Distort("Distort", Range(0, 1000)) = 1.0
			
	}
		SubShader
		{
			Tags{ "Queue" = "Overlay" "IgnoreProjector" = "True" "RenderType" = "Transparent" }

			GrabPass{ "_GrabTexture" }
			Pass
			{
				Lighting Off ZWrite On
				Blend SrcAlpha OneMinusSrcAlpha
				Cull Back

				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma target 2.0
				#include "UnityCG.cginc"

				struct appdata
				{
					fixed4 vertex : POSITION;
					fixed4 normal : NORMAL;
					fixed3 uv : TEXCOORD0;
				};

				struct v2f
				{
					fixed4 vertex : SV_POSITION;
					fixed3 rimColor : TEXCOORD0;
					fixed4 screenPos : TEXCOORD1;
					fixed3 colorComp : COLOR;
					fixed3 fresnelComp : TEXCOORD2;
				};

				sampler2D _GrabTexture;
				fixed4 _Color, _GrabTexture_TexelSize;
				fixed _Fresnel, _FresnelWidth, _Distort;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);

					//fresnel 
					fixed3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
					fixed dotProduct = 1 - saturate(dot(v.normal, viewDir));
					o.rimColor.rgb = smoothstep(1 - _FresnelWidth, 1.0h, dotProduct) * 0.5h;
					o.screenPos = ComputeScreenPos(o.vertex);
					
					//optimimation
					o.screenPos.xy += _Distort * _GrabTexture_TexelSize.xy;
					o.colorComp = _Color * _Color.a + 1;
					o.fresnelComp = _Color * pow(_Fresnel, o.rimColor);
					return o;
				}

				fixed4 frag(v2f i, fixed face : VFACE) : SV_Target
				{
					fixed3 distortColor = tex2Dproj(_GrabTexture, i.screenPos) * i.colorComp;
					return fixed4(lerp(distortColor, i.fresnelComp, i.rimColor.r), 0.9h);
				}
				ENDCG
			}
		}
}
