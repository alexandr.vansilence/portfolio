Shader "Unlit/Portal"
{
    Properties
    {
        _DistortionTex ("Distortion texture", 2D) = "black" {}
        _DistortionMask("Distortion mask", 2D) = "black" {}
        _Speed("Speed distortion", Float) = 0
        _MainTex("Portal texture", 2D) = "black" {}
        _ColorRamp("Color ramp", Color) = (1,1,1,1)
        _RampTex("Ramp texture", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

        GrabPass
        {
            "_BackgroundTexture"
        }

        Pass
        {
            Cull off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD1;
                float4 mainuv : TEXCOORD2;
            };

            sampler2D _DistortionTex;
            float4 _DistortionTex_ST;

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _RampTex;
            float4 _RampTex_ST;

            sampler2D _DistortionMask;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _DistortionTex);
                o.screenPos = ComputeGrabScreenPos(o.vertex);
                o.mainuv.xy = TRANSFORM_TEX(v.uv, _MainTex);
                o.uv.zw = TRANSFORM_TEX(v.uv, _RampTex);
                o.mainuv.zw = (float2)(_Speed * _Time.y);
                return o;
            }

            
            sampler2D _BackgroundTexture;
            fixed4 _ColorRamp;

            fixed4 frag(v2f i) : SV_Target
            {
                fixed4 mask = tex2D(_DistortionMask, i.mainuv.xy);
                fixed4 ramp = tex2D(_RampTex, i.uv.zw);
                fixed4 tex  = tex2D(_MainTex, i.mainuv.xy + i.mainuv.zw);
                fixed4 dist = tex2D(_DistortionTex,i.uv.xy + i.mainuv.zw);

                fixed4 col = tex2Dproj(_BackgroundTexture, i.screenPos + (dist * mask.r));

                fixed4 portal = lerp(col, tex, tex.r);
                fixed4 final = lerp(portal, _ColorRamp, ramp.r);

                return final;
            }

            ENDCG
        }
    }
}
