﻿Shader "HPEW/ShieldShader"
{
	Properties
	{
		_ShieldColor("Shield Color", Color) = (0,0,0,1)
		_MainTex("Transparency Mask", 2D) = "white" {}
		_ShieldIntensity("Shield Intensity", Range(0,1)) = 1

		_ShieldMoveness("Shield Field Moveness", Range(0,2)) = 1

		_Bias("Bias", Float)   = 1
		_Scale("Scale", Float) = 1
		_Power("Power", Float) = 1

	}

	SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }

		ZWrite off
	    Cull off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float intensity : COLOR0;
				float4 vertex : SV_POSITION;
			};

			float _Bias;
			float _Scale;
			float _Power;

			float _ShieldIntensity;
			float4 _ShieldColor;

			sampler2D _MainTex;
			float4 _MainTex_ST;

			float _ShieldMoveness;
		

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex) + _Time.x * _ShieldMoveness;
				float4 worldVertex = mul(unity_ObjectToWorld, v.vertex);

				float3 normWorld = normalize(mul(unity_ObjectToWorld, v.normal));
				float3 I = normalize(worldVertex - _WorldSpaceCameraPos.xyz);
				float fresnel = 0;
				float dt = dot(I, normWorld);
				fresnel = saturate(_Bias + _Scale * pow(1.0 - dt * dt, _Power));

				
				o.intensity = fresnel + _ShieldIntensity;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 transparentyMask = tex2D(_MainTex, i.uv);
			    return fixed4(_ShieldColor.r, _ShieldColor.g, _ShieldColor.b, saturate(transparentyMask.r * i.intensity));
		    }

		    ENDCG
	    }
	}
}
